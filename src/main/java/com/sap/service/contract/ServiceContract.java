package com.sap.service.contract;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sap.service.model.Service;

@Path("/services/")
@RestController
@Produces("application/json")
@Consumes("application/json")
public interface ServiceContract {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create")
	public Long create(@RequestBody Service service);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/read/{id}")
	public Service read(@PathParam(value = "id") long id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/readAll")
	public List<Service> readAll();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/update")
	public Long update(@RequestBody Service service);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delete/{id}")
	public Service delete(@PathParam(value = "id") long id);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/echo/{echo}")
	public String echo(@PathParam(value="echo") String echo);

}
