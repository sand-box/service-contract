package com.sap.service.model;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="T_Service")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Service {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Basic
	private String name;
	@Basic
	private String description;
	@Basic
	private String note;
	@Basic
	private Double estimatedDuration;
	@Basic
	private Double setFee;
	@Basic
	private Double feePerUnit;
	@Basic
	private Double totalCost;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Double getEstimatedDuration() {
		return estimatedDuration;
	}

	public void setEstimatedDuration(Double estimatedDuration) {
		this.estimatedDuration = estimatedDuration;
	}

	public Double getSetFee() {
		return setFee;
	}

	public void setSetFee(Double setFee) {
		this.setFee = setFee;
	}

	public Double getFeePerUnit() {
		return feePerUnit;
	}

	public void setFeePerUnit(Double feePerUnit) {
		this.feePerUnit = feePerUnit;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public Service(String name, String description, String note,
			Double estimatedDuration, Double setFee, Double feePerUnit,
			Double totalCost) {
		super();
		this.name = name;
		this.description = description;
		this.note = note;
		this.estimatedDuration = estimatedDuration;
		this.setFee = setFee;
		this.feePerUnit = feePerUnit;
		this.totalCost = totalCost;
	}

	public Service() {
		
	}
	
	@Override
	public String toString() {
		return "Service [id=" + id + ", name=" + name + ", description="
				+ description + ", note=" + note + ", estimatedDuration="
				+ estimatedDuration + ", setFee=" + setFee + ", feePerUnit="
				+ feePerUnit + ", totalCost=" + totalCost + "]";
	}
	
	
}
